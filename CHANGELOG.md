# Change Log

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [Unreleased]

### Added
… for new features.

### Changed
… for changes in existing functionality.

### Deprecated
… for once-stable features removed in upcoming releases.

### Removed
… for deprecated features removed in this release.

### Fixed
… for any bug fixes.

### Security
… to invite users to upgrade in case of vulnerabilities.

[Unreleased]: https://gitlab.com/hugocf/sandbox/compare/3bea81...master
